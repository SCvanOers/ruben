import {Point} from 'pixi';
import TweenMax from 'TweenMax'

export default class DynamicBackground {


  constructor() {
    window.addEventListener("resize", this.onWindowResize.bind(this))

    this.init();

    // Start animations
    this.animate();
  }

  init() {

    const HEIGHT = this.height = window.innerHeight;
    const WIDTH = this.width = window.innerWidth;
    this.ratio = WIDTH / HEIGHT;

    // Renderer
    this.renderer = PIXI.autoDetectRenderer(WIDTH, HEIGHT, {transparent: true});
    document.body.appendChild(this.renderer.view);

    // Stage
    this.stage = new PIXI.Container();

    this.stage.interactive = true;

    this.stage.addChild(this.createNoise());
    this.stage.addChild(this.createSlider());

  }

  /**
   *
   */
  onWindowResize() {

    //this.init();

  }

  /**
   * Show the noise
   */
  showNoise() {
    TweenMax.to(this.noise, 1, {alpha: 1});

  }

  /**
   * Hide the noise.
   */
  hideNoise() {
    TweenMax.to(this.noise, 1, {alpha: 0});
  }

  /**
   *
   * @returns {.exports.slider|*}
   */
  createNoise() {
    const noiseImage = PIXI.Texture.fromImage(require("assets/images/noise.png"));
    this.noise = new PIXI.TilingSprite(noiseImage, this.width * 1.1, this.height * 1.1);
    this.noise.x = - this.width * .5;
    this.noise.y = - this.height * .5;
    this.noise.tileScale = new PIXI.Point(.9, .9);
    return this.noise;
  }

  /**
   * Show slider.
   */
  showSlider() {
    TweenMax.to(this.slider, 1, {alpha: 1});
  }

  /**
   * Hide slider
   */
  hideSlider() {
    TweenMax.to(this.slider, 1, {alpha: 0});
  }


  /**
   * Slider create
   * @returns {.exports.slider|*}
   */
  createSlider() {
    this.slider = new PIXI.Container();
    this.slider.alpha = 0;
    this.sliderFactor = 7.5;
    this.sliderSize = (this.width / 10) * this.sliderFactor;
    if(this.sliderSize > (this.height / 10) * this.sliderFactor) {
      this.sliderSize = (this.height  / 10) * this.sliderFactor;
    }

    this.sliderImage = PIXI.Sprite.fromImage(require("assets/images/slider.jpg"));
    this.sliderImage.x = this.width / 2;
    this.sliderImage.y = this.height  / 2;
    this.sliderImage.width = this.sliderSize * 5;
    this.sliderImage.height = this.sliderSize;
    this.sliderImage.anchor = new PIXI.Point(0.1, .5);

    this.sliderMask = PIXI.Sprite.fromImage(require('assets/images/mask-white.png'));
    this.sliderMask.x = this.width / 2;
    this.sliderMask.y = this.height  / 2;
    this.sliderMask.width = this.sliderSize;
    this.sliderMask.height = this.sliderSize;
    this.sliderMask.anchor.set(0.5);

    this.sliderImage.mask = this.sliderMask;
    this.slider.addChild(this.sliderImage);
    this.slider.addChild(this.sliderMask);
    return this.slider;
  }

  /**
   * Update slider size
   * @param index
   */
  updateSliderSize() {

    this.sliderSize = (window.innerWidth / 10) * this.sliderFactor;
    if(this.sliderSize > (window.innerHeight / 10) * this.sliderFactor) {
      this.sliderSize = (window.innerHeight  / 10) * this.sliderFactor;
    }
    
    this.sliderImage.width = this.sliderSize * 5;
    this.sliderImage.height = this.sliderSize;
    this.sliderMask.width = this.sliderSize;
    this.sliderMask.height = this.sliderSize;
  }

  /**
   * Animate to slider image
   * @param index
   */
  goToSliderImage(index) {
    const WIDTH = this.width = window.innerWidth;
    TweenMax.to(this.sliderImage, 1, {x: -this.sliderSize * index + (WIDTH / 2), ease: TweenMax.Power3.easeOut});
    TweenMax.to(this.sliderMask, .5, {width:  this.sliderSize * .95, height: this.sliderSize * .95, ease: TweenMax.Power3.easeOut});
    TweenMax.to(this.sliderMask, .5, {width:  this.sliderSize * 1, height: this.sliderSize * 1, delay: .4, ease: TweenMax.Power3.easeOut});
  }

  /**
   * Animate it.
   */
  animate() {
    this.noise.x = - this.width * .1 + Math.random() * 50;
    this.noise.y = - this.height * .1 + Math.random() * 50;

    // Loop animation
    requestAnimationFrame(() => {
      this.animate();
    });


    // Re-render
    this.renderer.render(this.stage);

  }

}