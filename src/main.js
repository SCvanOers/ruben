import Vue from 'vue'
import VueRouter from 'vue-router'
import HeaderComponent from './components/header'
import DynamicBackground from './DynamicBackground'
window.dynamic = new DynamicBackground()

/**
 * Base styles.
 */
import './elements/base.scss'

/**
 * Create sprite sheet.
 */
const sprite = require.context('assets/sprite', true, /\.svg$/)
sprite.keys().map(sprite);

/**
 * Tell vue to use the VueRouter
 */
Vue.use(VueRouter)

/**
 * Page templates
 */
const homeTemplate = require('./pages/home.vue')
const sliderTemplate = require('./pages/slider.vue')
const caseTemplate = require('./pages/case.vue')
const aboutTemplate = require('./pages/about.vue')

/**
 * Router
 */
const router = new VueRouter({
  routes: [
    {path: '/intro', component: homeTemplate},
    {path: '/slider', component: sliderTemplate},
    {path: '/case/:name', component: caseTemplate},
    {path: '/about', component: aboutTemplate}
  ]
})

/**
 * The Vue instance
 */
new Vue({
  el: "main",
  router,
  components: { HeaderComponent }
}).$mount('main')

